# [1.0.0-qa.2](https://gitlab.com/rampanwar1/nodejs/compare/v1.0.0-qa.1...v1.0.0-qa.2) (2021-09-23)


### Bug Fixes

* dockerfile ([f9d2470](https://gitlab.com/rampanwar1/nodejs/commit/f9d2470d8b8429eebb970b5f2965330a76cf1b63))

# 1.0.0-qa.1 (2021-09-23)


### Bug Fixes

* .releaserc ([01f973e](https://gitlab.com/rampanwar1/nodejs/commit/01f973e703c4571577d4f3b44d80aad6939ca267))
* .releaserc ([e2575de](https://gitlab.com/rampanwar1/nodejs/commit/e2575def14031481719b93f6624e8b4afe6cca1c))
* .releaserc ([8909868](https://gitlab.com/rampanwar1/nodejs/commit/8909868206c230d80976ff003ed403332824ef5a))
* .releaserc ([5491ef2](https://gitlab.com/rampanwar1/nodejs/commit/5491ef296858e552f86de7f8d094d3c7632caf43))
* .releaserc ([0d3e09a](https://gitlab.com/rampanwar1/nodejs/commit/0d3e09a7c924411867c7ee6cc4348f6e0dbd6024))
* .releaserc ([351306b](https://gitlab.com/rampanwar1/nodejs/commit/351306b023872d8d05e81f45aa4b5973e17d502d))
* add new branch ([39751fb](https://gitlab.com/rampanwar1/nodejs/commit/39751fbfbae00658537e641f8937ed42c5ce08af))
* add new branch ([cc739eb](https://gitlab.com/rampanwar1/nodejs/commit/cc739eb1dcd0ce90b8c92f4b7aa08dd8062d7dde))
* add new branch ([d44a74c](https://gitlab.com/rampanwar1/nodejs/commit/d44a74c63c7f963eac166314af451c79660640e4))
* add new branch ([b939544](https://gitlab.com/rampanwar1/nodejs/commit/b939544965dc941a8a6eb0bf8edd4383cc127ad4))
* add new branch ([a741470](https://gitlab.com/rampanwar1/nodejs/commit/a7414708f847c0d8c47ff5d537d9efe4024ffad6))
* Add something in dockerfile ([df6ea21](https://gitlab.com/rampanwar1/nodejs/commit/df6ea212e786e63c7b383efe1b33903658ad5fe0))
* dockerfile ([d0aeb8a](https://gitlab.com/rampanwar1/nodejs/commit/d0aeb8af7e66e2c72aa500f2aba1e89972245949))
* dockerfile ([224fa9b](https://gitlab.com/rampanwar1/nodejs/commit/224fa9b8ccccac40f3e221b7e0d3f6a5f9d89938))
* dockerfile ([26b0c8c](https://gitlab.com/rampanwar1/nodejs/commit/26b0c8c736e9e4679ab379c8c0575012bef72bec))
* dockerfile ([d53cda8](https://gitlab.com/rampanwar1/nodejs/commit/d53cda8ba63b5c95fc527059559ab9cd1b5400ec))
* dockerfile ([bd95ddf](https://gitlab.com/rampanwar1/nodejs/commit/bd95ddf1acfb39701c1fdda42bd5c36a35630108))
* dockerfile ([a62cf2b](https://gitlab.com/rampanwar1/nodejs/commit/a62cf2b39cde29f1b824357f1a740dd55c6ff3ac))
* dockerfile ([fa309e3](https://gitlab.com/rampanwar1/nodejs/commit/fa309e34db600cc4181c85b923e9baeb8e8655db))
* dockerfile ([a842912](https://gitlab.com/rampanwar1/nodejs/commit/a8429123b453c420c690b7b6b24536e2c83eac39))
* dockerfile ([3f3d452](https://gitlab.com/rampanwar1/nodejs/commit/3f3d452413e73a8c44d5a0dd4cb572809b3fa96c))
* dockerfile ([644460e](https://gitlab.com/rampanwar1/nodejs/commit/644460ea2a3ea0c0c26c73be35ccd7eda469028a))
* dockerfile ([82a0cf3](https://gitlab.com/rampanwar1/nodejs/commit/82a0cf331f81acb425629a7241ac7d1716aa03ec))
* dockerfile ([96500c5](https://gitlab.com/rampanwar1/nodejs/commit/96500c5d198384f0bec5d364a1c04fe5a362673a))
* dockerfile ([fb1869a](https://gitlab.com/rampanwar1/nodejs/commit/fb1869a6f386b77231ff7405daf1ab23ded1d943))
* dockerfile ([0c0e657](https://gitlab.com/rampanwar1/nodejs/commit/0c0e657fc2062628c0145b45820baef40067aacc))
* fix dockerfile ([4ab0817](https://gitlab.com/rampanwar1/nodejs/commit/4ab0817c48b701b62c338ad782444a9c728ebe6b))


### Features

* Add something in dockerfile ([a4edc12](https://gitlab.com/rampanwar1/nodejs/commit/a4edc12e5cada7296d15ab51bfff6fc13a8b953c))
* Add something in dockerfile ([c9d2d19](https://gitlab.com/rampanwar1/nodejs/commit/c9d2d1951e8e62d32ab3b86f4bf8db173116cb87))
* dockerfile ([aae6618](https://gitlab.com/rampanwar1/nodejs/commit/aae661885d76dce1bde707f7cdf931a0e1a59584))

# [1.0.0-develop.7](https://gitlab.com/rampanwar1/nodejs/compare/v1.0.0-develop.6...v1.0.0-develop.7) (2021-09-22)


### Features

* dockerfile ([aae6618](https://gitlab.com/rampanwar1/nodejs/commit/aae661885d76dce1bde707f7cdf931a0e1a59584))

# [1.0.0-develop.6](https://gitlab.com/rampanwar1/nodejs/compare/v1.0.0-develop.5...v1.0.0-develop.6) (2021-09-22)


### Bug Fixes

* .releaserc ([01f973e](https://gitlab.com/rampanwar1/nodejs/commit/01f973e703c4571577d4f3b44d80aad6939ca267))

# [1.0.0-develop.5](https://gitlab.com/rampanwar1/nodejs/compare/v1.0.0-develop.4...v1.0.0-develop.5) (2021-09-22)


### Bug Fixes

* .releaserc ([e2575de](https://gitlab.com/rampanwar1/nodejs/commit/e2575def14031481719b93f6624e8b4afe6cca1c))
* .releaserc ([8909868](https://gitlab.com/rampanwar1/nodejs/commit/8909868206c230d80976ff003ed403332824ef5a))

# [1.0.0-develop.4](https://gitlab.com/rampanwar1/nodejs/compare/v1.0.0-develop.3...v1.0.0-develop.4) (2021-09-22)


### Bug Fixes

* .releaserc ([5491ef2](https://gitlab.com/rampanwar1/nodejs/commit/5491ef296858e552f86de7f8d094d3c7632caf43))

# [1.0.0-develop.3](https://gitlab.com/rampanwar1/nodejs/compare/v1.0.0-develop.2...v1.0.0-develop.3) (2021-09-22)


### Bug Fixes

* .releaserc ([0d3e09a](https://gitlab.com/rampanwar1/nodejs/commit/0d3e09a7c924411867c7ee6cc4348f6e0dbd6024))

# [1.0.0-develop.2](https://gitlab.com/rampanwar1/nodejs/compare/v1.0.0-develop.1...v1.0.0-develop.2) (2021-09-22)


### Bug Fixes

* .releaserc ([351306b](https://gitlab.com/rampanwar1/nodejs/commit/351306b023872d8d05e81f45aa4b5973e17d502d))

# 1.0.0-develop.1 (2021-09-22)


### Bug Fixes

* add new branch ([39751fb](https://gitlab.com/rampanwar1/nodejs/commit/39751fbfbae00658537e641f8937ed42c5ce08af))
* add new branch ([cc739eb](https://gitlab.com/rampanwar1/nodejs/commit/cc739eb1dcd0ce90b8c92f4b7aa08dd8062d7dde))
* add new branch ([d44a74c](https://gitlab.com/rampanwar1/nodejs/commit/d44a74c63c7f963eac166314af451c79660640e4))
* add new branch ([b939544](https://gitlab.com/rampanwar1/nodejs/commit/b939544965dc941a8a6eb0bf8edd4383cc127ad4))
* add new branch ([a741470](https://gitlab.com/rampanwar1/nodejs/commit/a7414708f847c0d8c47ff5d537d9efe4024ffad6))
* dockerfile ([d0aeb8a](https://gitlab.com/rampanwar1/nodejs/commit/d0aeb8af7e66e2c72aa500f2aba1e89972245949))
* dockerfile ([224fa9b](https://gitlab.com/rampanwar1/nodejs/commit/224fa9b8ccccac40f3e221b7e0d3f6a5f9d89938))
* dockerfile ([26b0c8c](https://gitlab.com/rampanwar1/nodejs/commit/26b0c8c736e9e4679ab379c8c0575012bef72bec))
* dockerfile ([d53cda8](https://gitlab.com/rampanwar1/nodejs/commit/d53cda8ba63b5c95fc527059559ab9cd1b5400ec))
* dockerfile ([bd95ddf](https://gitlab.com/rampanwar1/nodejs/commit/bd95ddf1acfb39701c1fdda42bd5c36a35630108))
* dockerfile ([a62cf2b](https://gitlab.com/rampanwar1/nodejs/commit/a62cf2b39cde29f1b824357f1a740dd55c6ff3ac))
* dockerfile ([fa309e3](https://gitlab.com/rampanwar1/nodejs/commit/fa309e34db600cc4181c85b923e9baeb8e8655db))
* dockerfile ([a842912](https://gitlab.com/rampanwar1/nodejs/commit/a8429123b453c420c690b7b6b24536e2c83eac39))
* dockerfile ([3f3d452](https://gitlab.com/rampanwar1/nodejs/commit/3f3d452413e73a8c44d5a0dd4cb572809b3fa96c))
* dockerfile ([644460e](https://gitlab.com/rampanwar1/nodejs/commit/644460ea2a3ea0c0c26c73be35ccd7eda469028a))
* dockerfile ([82a0cf3](https://gitlab.com/rampanwar1/nodejs/commit/82a0cf331f81acb425629a7241ac7d1716aa03ec))
* dockerfile ([96500c5](https://gitlab.com/rampanwar1/nodejs/commit/96500c5d198384f0bec5d364a1c04fe5a362673a))
* dockerfile ([fb1869a](https://gitlab.com/rampanwar1/nodejs/commit/fb1869a6f386b77231ff7405daf1ab23ded1d943))
* dockerfile ([0c0e657](https://gitlab.com/rampanwar1/nodejs/commit/0c0e657fc2062628c0145b45820baef40067aacc))

# 1.0.0 (2021-09-14)


### Bug Fixes

* dockerfile ([0c0e657](https://gitlab.com/rampanwar1/nodejs/commit/0c0e657fc2062628c0145b45820baef40067aacc))
